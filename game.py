# Here we will have the user interact with the computer.
# the goal is for the computer to guess the user's age within 
# five attempts. If guessed correctly programs ends.
# If failed all 5 attempts program ends.

from random import randint
print("Hello user!")
name = input("What is your name? ")
print("Hello", name)
print(name, "We are going to be playing a game. If I guess your birthday you're free to go home.. are you ready? No pressure...")

guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2022)

# Here we will perform guess 1
print("Guess 1 :" , name, "were you born in",
      month_number, "/", year_number, "?")

# We will ask for the user to respond if that is correct
response = input("yes or no? ").lower()

if response == "yes":
    print("Aww shucks... you're free to go.. GG's")
    exit()
    
else:
    print("Drat! Lemme try again!")

#Guess 2

month_number = randint(1, 12)
year_number = randint(1924, 2022)          

if response == "no":
    print("Guess 2 :" , name, "were you born in",
    month_number, "/", year_number, "?")
    
# We will ask for the user to respond if that is correct
    
response = input("yes or no? ").lower()

if response == "yes":
    print("Aww shucks... you're free to go.. GG's")
    exit()
    
else:
    print("Drat! Lemme try again!")
    
#Guess 3

month_number = randint(1, 12)
year_number = randint(1924, 2022)   

if response == "no":
    print("Guess 3 :" , name, "were you born in",
    month_number, "/", year_number, "?")
    
# We will ask for the user to respond if that is correct

response = input("yes or no? ").lower()

if response == "yes":
    print("Aww shucks... you're free to go.. GG's")
    exit()
    
else:
    print("Drat! Lemme try again!")
    
#Guess 4

month_number = randint(1, 12)
year_number = randint(1924, 2022)   

if response == "no":
    print("Guess 4 :" , name, "were you born in",
    month_number, "/", year_number, "?")
    
# We will ask for the user to respond if that is correct

response = input("yes or no? ").lower()

if response == "yes":
    print("Aww shucks... you're free to go.. GG's")
    exit()
    
else:
    print("Drat! Lemme try again!")
    
#Guess 5

month_number = randint(1, 12)
year_number = randint(1924, 2022)   

if response == "no":
    print("Guess 5 :" , name, "were you born in",
    month_number, "/", year_number, "?")
    
# We will ask for the user to respond if that is correct

response = input("yes or no? ").lower()

if response == "yes":
    print("Aww shucks... you're free to go.. GG's")
    exit()
    
else:
    print("I have other things to do.. GAME OVER.")